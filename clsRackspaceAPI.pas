unit clsRackspaceAPI;

interface

type
  // *** -------------------------------------------------------------------------
  // *** CLASS: TRackspaceAPI
  // *** -------------------------------------------------------------------------
  /// This class is a wrapper around the Rackspace API
  TRackspaceAPI = class( TObject )
    private
      FAuthenticateURL: string;
      FUsername       : string;
      FAPIKey         : string;
      FAccount        : string;
      FAuthToken      : string;
      FAPIURL         : string;
    public
      constructor Create();
      destructor Destroy; override;

      // *** Authentication
      /// This will do an authentication request to retrieve the Account and AuthToken values
      //  and requires the UserName and APIKey to be supplied. If you have retrieved the Account
      //  AuthToken already, simply set them before calling any of the other functions.
      function DoAuthenticationRequest(): Boolean;

      // *** Images
      function ListImages ( const AServerID: string = ''; const ADetailed: Boolean = False ): string;
      function DeleteImage( const AImageID: string ): Boolean;
      function CreateImage( const AServerID: string; const AName: string ): Boolean;

      /// This will list the images that have created for a server, and sort them according to the
      //  creation time, and then delete the ealiest so that only ANumberToKeep are left on the
      //  server.
      function KeepImagesForServer( const AserverID: string; const ANumberToKeep: Integer = 7 ): Boolean;

      // *** Servers
      function ListServers( const AServerName: string = '' ): string;

      // *** URL Properties
      property AuthenticateURL : string read FAuthenticateURL write FAuthenticateURL;
      property APIURL          : string read FAPIURL          write FAPIURL;

      // ** Authenication Properties
      property UserName        : string read FUsername        write FUsername;
      property APIKey          : string read FAPIKey          write FAPIKey;
      property Account         : string read FAccount         write FAccount;
      property AuthToken       : string read FAuthToken       write FAuthToken;
  end;

const
  /// These are the default URLs. They can be overridden in the instance of the class by setting the
  //  AuthenticateURL and APIURL properties.
  C_RAX_AUTH_URL = 'https://identity.api.rackspacecloud.com/v2.0/tokens';
  C_RAX_API_URL  = 'https://dfw.servers.api.rackspacecloud.com/v2/';

implementation

uses
  SynCommons,
  SynCrtSock  ,
  Classes;

{ TRackspaceAPI }


{ TRackspaceAPI }

{-------------------------------------------------------------------------------
  Procedure: TRackspaceAPI.Create
  Author:    bvonfintel
  DateTime:  2014.09.29
  Arguments: None
  Result:    None
-------------------------------------------------------------------------------}
constructor TRackspaceAPI.Create;
begin
  inherited;
  FAuthenticateURL := C_RAX_AUTH_URL;
  FAPIURL          := C_RAX_API_URL;
end;


{-------------------------------------------------------------------------------
  Procedure: TRackspaceAPI.CreateImage
  Author:    bvonfintel
  DateTime:  2014.09.29
  Arguments: const AServerID, AName: string
  Result:    Boolean
-------------------------------------------------------------------------------}
function TRackspaceAPI.CreateImage(const AServerID, AName: string): Boolean;
var
  LURI          : TURI;
  LHttpClient   : TWinINet;
  LEndPoint     : string;
  LRequestJSON  : Variant;
  LResponseJSON : Variant;
  LInData       : string;
  LInHeaders    : string;
  LOutData      : string;
  LOutHeaders   : string;
  LResponseCode  : Integer;
begin
  Result := False;
  TSynLog.Add.Log( sllInfo, '[TRackspaceAPI.CreateImage] Creating Image : % for server: %' , [ AName, AServerID ] );
  TSynLog.Add.Log( sllInfo, '[TRackspaceAPI.CreateImage] Using API URL : %' , [  FAPIUrl ] );
  LURI.From( FAPIURL );

  LEndPoint := FAccount + '/servers/' + AServerID + '/action';
  LInHeaders := 'X-Auth-Token: ' + FAuthToken;

  LRequestJSON := _ObjFast( [ 'createImage', _ObjFast( [ 'name', AName ] ) ] );
  LInData := LRequestJSON;

  LHttpClient := TWinINet.Create( LURI.Server, LURI.Port, LURI.Https );
  try
    TSynLog.Add.Log( sllInfo, '[TRackspaceAPI.CreateImage] Endpoint : %' , [  LURI.Address + LEndPoint] );

    LResponseCode := LHttpClient.Request( LURI.Address + LEndPoint, 'POST', 0, LInHeaders, LInData, 'application/json', LOutHeaders, LOutData  );
    if ( ( LResponseCode >= 200 ) and ( LResponseCode < 300 ) ) then begin
      Result := True;
    end;

    TSynLog.Add.Log( sllDebug, '[TRackspaceAPI.CreateImage] Response (%) [%] % ' , [ LResponseCode, LOutHeaders, LOutData ] );

  finally
    LHttpClient.Free();
  end;
end;

{-------------------------------------------------------------------------------
  Procedure: TRackspaceAPI.DeleteImage
  Author:    bvonfintel
  DateTime:  2014.09.29
  Arguments: const AImageID: string
  Result:    Boolean
-------------------------------------------------------------------------------}
function TRackspaceAPI.DeleteImage(const AImageID: string): Boolean;
var
  LURI          : TURI;
  LHttpClient   : TWinINet;
  LEndPoint     : string;
  LRequestJSON  : Variant;
  LResponseJSON : Variant;
  LInData       : string;
  LInHeaders    : string;
  LOutData      : string;
  LOutHeaders   : string;
  LResponseCode  : Integer;
begin
  Result := False;
  TSynLog.Add.Log( sllInfo, '[TRackspaceAPI.DeleteImage] Deleting image : %' , [ AImageID ] );

  TSynLog.Add.Log( sllInfo, '[TRackspaceAPI.DeleteImage] Using Api Url : %' , [ FAPIUrl ] );
  LURI.From( FAPIURL );

  LEndPoint := FAccount + '/images/' + AImageID;
  LInHeaders := 'X-Auth-Token: ' + FAuthToken;

  LHttpClient := TWinINet.Create( LURI.Server, LURI.Port, LURI.Https );
  try
    LResponseCode := LHttpClient.Request( LURI.Address + LEndPoint, 'DELETE', 0, LInHeaders, '', 'application/json', LOutHeaders, LOutData  );
    if ( ( LResponseCode >= 200 ) and ( LResponseCode < 300 ) ) then
      Result := True;
      
    TSynLog.Add.Log( sllDebug, '[TRackspaceAPI.DeleteImage] Response (%) : %' , [ LResponseCode, LOutData ] );
  finally
    LHttpClient.Free();
  end;
end;

{-------------------------------------------------------------------------------
  Procedure: TRackspaceAPI.Destroy
  Author:    bvonfintel
  DateTime:  2014.09.29
  Arguments: None
  Result:    None
-------------------------------------------------------------------------------}
destructor TRackspaceAPI.Destroy;
begin
  inherited;
end;

{-------------------------------------------------------------------------------
  Procedure: TRackspaceAPI.DoAuthenticationRequest
  Author:    bvonfintel
  DateTime:  2014.09.29
  Arguments: None
  Result:    Boolean
-------------------------------------------------------------------------------}
function TRackspaceAPI.DoAuthenticationRequest: Boolean;
var
  LURI          : TURI;
  LHttpClient   : TWinINet;
  LRequestJSON  : Variant;
  LResponseJSON : Variant;
  LInData       : string;
  LOutData      : string;
  LOutHeaders   : string;
  LResponseCode  : Integer;
begin
  Result := False;
  
  LURI.From( FAuthenticateURL );
  LRequestJSON := _ObjFast( [ 'auth',
                              _ObjFast( [ 'RAX-KSKEY:apiKeyCredentials',
                                           _ObjFast( ['username', FUsername,
                                                      'apiKey', FAPIKey ] ) ] ) ] );
  LInData := LRequestJSON;

  LHttpClient := TWinINet.Create( LURI.Server, LURI.Port, LURI.Https );
  try
    LResponseCode := LHttpClient.Request( LURI.Address, 'POST', 0, '', LInData, 'application/json', LOutHeaders, LOutData );
    if ( ( LResponseCode >= 200 ) and ( LResponseCode < 300 ) ) then begin
      TSynLog.Add.Log( sllDebug, '[TRackspaceAPI.DoAuthenticationRequest] Response : %' , [ LOutData ] );

      LResponseJSON := _JsonFast( LOutData );

      FAccount    := LResponseJSON.access.token.tenant.id ;
      FAuthToken  := LResponseJSON.access.token.id ;

      Result := True;
    end;
  finally
    LHttpClient.Free();
  end;
end;

{-------------------------------------------------------------------------------
  Procedure: TRackspaceAPI.KeepImagesForServer
  Author:    bvonfintel
  DateTime:  2014.09.29
  Arguments: const AserverID: string; const ANumberToKeep: Integer
  Result:    Boolean
-------------------------------------------------------------------------------}
function TRackspaceAPI.KeepImagesForServer(const AserverID: string; const ANumberToKeep: Integer): Boolean;
var
  LImages : Variant;
  LTest   : TStringList;
  LIndex  : Integer;
begin
  LImages := _JsonFast( ListImages( AserverID, True ) );
  LTest := TStringList.Create();
  LTest.Sorted := True;

  for LIndex := 0 to LImages.images._Count - 1 do begin
    TSynLog.Add.Log( sllInfo, '[TRackspaceAPI.KeepImagesForServer] Found Image [%] : %' , [ LImages.images._(LIndex).id, LImages.images._(LIndex).created ] );
    LTest.AddObject( LImages.images._(LIndex).created, TObject( DocVariantData( LImages.images._(LIndex) ) ) );
  end;

  while LTest.Count > ANumberToKeep do begin
    TSynLog.Add.Log( sllInfo, '[TRackspaceAPI.KeepImagesForServer] Too many images, removing : %' , [ PDocVariantData( LTest.Objects[0] ).GetValueByPath( 'id' ) ] );
    DeleteImage( PDocVariantData( LTest.Objects[0] ).GetValueByPath( 'id' ) );
    LTest.Delete(0);
  end;


end;

{-------------------------------------------------------------------------------
  Procedure: TRackspaceAPI.ListImages
  Author:    bvonfintel
  DateTime:  2014.09.29
  Arguments: const AServerID: string
  Result:    string
-------------------------------------------------------------------------------}
function TRackspaceAPI.ListImages(const AServerID: string; const ADetailed: Boolean ): string;
var
  LURI          : TURI;
  LHttpClient   : TWinINet;
  LEndPoint     : string;
  LRequestJSON  : Variant;
  LResponseJSON : Variant;
  LInData       : string;
  LInHeaders    : string;
  LOutData      : string;
  LOutHeaders   : string;
  LResponseCode  : Integer;
begin
  Result := '';

  TSynLog.Add.Log( sllInfo, '[TRackspaceAPI.ListImages] Using API URL : %' , [ FAPIUrl ] );
  LURI.From( FAPIURL );

  LEndPoint := FAccount + '/images';
  if ( ADetailed ) then
    LEndPoint := LEndPoint + '/detail';
  if ( AServerID <> '' ) then
    LEndPoint := LEndPoint + '?server=' + AServerID;

  LInHeaders := 'X-Auth-Token: ' + FAuthToken;

  LHttpClient := TWinINet.Create( LURI.Server, LURI.Port, LURI.Https );
  try
    LResponseCode := LHttpClient.Request( LURI.Address + LEndPoint, 'GET', 0, LInHeaders, '', 'application/json', LOutHeaders, LOutData  );
    if ( ( LResponseCode >= 200 ) and ( LResponseCode < 300 ) ) then
      Result := LOutData;

    TSynLog.Add.Log( sllDebug, '[TRackspaceAPI.ListImages] Response (%) : %' , [ LResponseCode, LOutData ] );
  finally
    LHttpClient.Free();
  end;
end;

{-------------------------------------------------------------------------------
  Procedure: TRackspaceAPI.ListServers
  Author:    bvonfintel
  DateTime:  2014.09.29
  Arguments: const AServerName: string
  Result:    string
-------------------------------------------------------------------------------}
function TRackspaceAPI.ListServers(const AServerName: string): string;
var
  LURI          : TURI;
  LHttpClient   : TWinINet;
  LEndPoint     : string;
  LRequestJSON  : Variant;
  LResponseJSON : Variant;
  LInData       : string;
  LInHeaders    : string;
  LOutData      : string;
  LOutHeaders   : string;
  LResponseCode  : Integer;
begin
  Result := '';

  LURI.From( FAPIURL );

  LEndPoint := FAccount + '/servers';
  if ( AServerName <> '' ) then
    LEndPoint := LEndPoint + '?name=' + AServerName;

  LInHeaders := 'X-Auth-Token: ' + FAuthToken;

  LHttpClient := TWinINet.Create( LURI.Server, LURI.Port, LURI.Https );
  try
    LResponseCode := LHttpClient.Request( LURI.Address + LEndPoint, 'GET', 0, LInHeaders, '', 'application/json', LOutHeaders, LOutData  );
    if ( ( LResponseCode >= 200 ) and ( LResponseCode < 300 ) ) then
      Result := LOutData;

  finally
    LHttpClient.Free();
  end;

end;

end.
